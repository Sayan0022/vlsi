
//import uvm_pkg::*; 

class ral_reg_register1 extends uvm_reg;
`uvm_object_utils(ral_reg_register1)
  rand uvm_reg_field rg1;

 function new(string name = "register1");
  super.new(name,8,UVM_NO_COVERAGE);
 endfunction

 virtual function void build();
   this.rg1 = uvm_reg_field::type_id::create("rg1");
   this.rg1.configure(this,8,0,"RW",0,1,0,1,1);
 endfunction 
 
endclass : ral_reg_register1



class ral_reg_register2 extends uvm_reg;
`uvm_object_utils(ral_reg_register2)
  rand uvm_reg_field rg2;

 function new(string name = "register2");
  super.new(name,8,UVM_NO_COVERAGE);
 endfunction

 virtual function void build();
   this.rg2 = uvm_reg_field::type_id::create("rg2");
   this.rg2.configure(this,8,0,"RW",0,1,0,1,1);
 endfunction 
 
endclass : ral_reg_register2


class reg_block extends uvm_reg_block;

`uvm_object_utils(reg_block)
  rand ral_reg_register1 rg_1;
  rand ral_reg_register2 rg_2;
   
   rand uvm_reg_field rg_1_rg1;
   rand uvm_reg_field rg1;
   rand uvm_reg_field rg_2_rg2;
   rand uvm_reg_field rg2;
  
 function new(string name = "reg_block");
  super.new(name,UVM_NO_COVERAGE);
 endfunction

 virtual function void build();
   this.rg_1 = ral_reg_register1::type_id::create("rg_1");
   this.rg_1.configure(this);
   this.rg_1.build();

   this.rg_2 = ral_reg_register2::type_id::create("rg_2");
   this.rg_2.configure(this);
   this.rg_2.build();
   this.default_map = create_map("default_map",8'h00,1,UVM_LITTLE_ENDIAN);
   this.default_map.add_reg(this.rg_1,8'h0,"RW");
   this.rg_1_rg1 = this.rg_1.rg1;
   this.rg1 = this.rg_1.rg1; 

   this.default_map.add_reg(this.rg_2,8'h1,"RW");
   this.rg_2_rg2 = this.rg_2.rg2;
   this.rg2 = this.rg_2.rg2; 
  
   lock_model();
 endfunction 

endclass






