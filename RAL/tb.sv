//`include "env.sv"
class tb extends uvm_test;

`uvm_component_utils(tb)

 env env_inst;


  function new(string name, uvm_component parent);
      super.new(name, parent);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new


virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    env_inst = env::type_id::create("env_inst",this);
   uvm_config_db#(virtual intf)::set(this,"*","vif",top.vif);
endfunction
endclass


class normal_test extends tb;

`uvm_component_utils(normal_test)
 
  function new(string name, uvm_component parent);
      super.new(name, parent);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new

  virtual task run_phase(uvm_phase phase);
     packet_sequence normal_seq;
     normal_seq  = packet_sequence::type_id::create("normal_seq");
     if(!normal_seq.randomize())
       `uvm_error("TRACE",$sformatf("%m")); 
     
     normal_seq.starting_phase = phase; 
     normal_seq.start(env_inst.agent_inst.sqr_inst);

     
  endtask

endclass

class reg_test extends tb;

`uvm_component_utils(reg_test)
  reg_block regmodel;
 
  function new(string name, uvm_component parent);
      super.new(name, parent);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new

  virtual task run_phase(uvm_phase phase);
     packet_reg_sequence reg_seq;
     phase.raise_objection(this);
     reg_seq  = packet_reg_sequence::type_id::create("reg_seq");
     reg_seq.regmodel = env_inst.regmodel;
     reg_seq.start(env_inst.agent_inst.sqr_inst);
    #100;

   phase.drop_objection(this);
     
  endtask

endclass

