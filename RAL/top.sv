module top;
reg clk,rst;
wire  write;
wire  [7:0]addr;
wire  [7:0]din;
wire [7:0]dout;

intf vif(clk);
add1 dut_inst(.din(vif.din),
              .dout(vif.dout),
              .clk(vif.clk),
              .rst(rst),
              .write(vif.write),
              .addr(vif.addr));

initial begin
clk =1;
rst=0;
#15 rst =1;
end

/*initial begin
$fsdbDumpvars();
end
*/
always #5 clk = !clk;
initial begin
  run_test("reg_test");
end


endmodule

