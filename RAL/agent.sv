//`include "packet_reg_sequence.sv"
//`include "drv.sv"
//`include "mon.sv"

typedef uvm_sequencer #(packet) sequencer;

class agent extends uvm_agent;
`uvm_component_utils(agent)

 uvm_analysis_port #(packet) an_port;
 
 drv drv_inst;
 sequencer sqr_inst;
 mon mon_inst;
  function new(string name, uvm_component parent);
      super.new(name, parent);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new


virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    an_port = new("an_port",this);
    drv_inst = drv::type_id::create("drv_inst",this);
    sqr_inst = sequencer::type_id::create("sqr_inst",this);
    mon_inst = mon::type_id::create("mon_inst",this);
endfunction

virtual function void connect_phase(uvm_phase phase);
   super.connect_phase(phase);
   drv_inst.seq_item_port.connect(sqr_inst.seq_item_export);
   mon_inst.an_port.connect(an_port);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
endfunction

endclass

