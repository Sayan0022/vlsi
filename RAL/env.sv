//`include "agent.sv"
//`include "adapter.sv"
typedef uvm_reg_predictor #(packet) reg_predictor;

class env extends uvm_env;
`uvm_component_utils(env)

 agent agent_inst;
 adapter adp_inst;
 reg_block regmodel;
 reg_predictor predict;
  function new(string name, uvm_component parent);
      super.new(name, parent);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new


virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    regmodel = reg_block::type_id::create("regmodel",this);
    regmodel.build();
    regmodel.lock_model();
    predict = reg_predictor::type_id::create("predict",this);
    adp_inst = adapter::type_id::create("adp_inst",this);
    agent_inst = agent::type_id::create("agent_inst",this);
    uvm_config_db#(reg_block)::set(this,"*","regmodel",regmodel);
endfunction

virtual function void connect_phase(uvm_phase phase);
   super.connect_phase(phase);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
   regmodel.default_map.set_sequencer(agent_inst.sqr_inst,adp_inst);
   regmodel.default_map.set_auto_predict(0); 
   predict.map = regmodel.default_map;
   predict.adapter = adp_inst;
   agent_inst.an_port.connect(predict.bus_in);
endfunction

endclass

