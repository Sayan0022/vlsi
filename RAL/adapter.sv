

class adapter extends uvm_reg_adapter;
   `uvm_object_utils( adapter )

   function new( string name = "" );
      super.new( name );
      supports_byte_enable = 0;
      provides_responses   = 0;
   endfunction: new
 
   virtual function uvm_sequence_item reg2bus( const ref uvm_reg_bus_op rw );
      packet pkt
        = packet::type_id::create("pkt");
      pkt.write = (rw.kind == UVM_WRITE) ? 1 : 0;
      pkt.addr = rw.addr;
      pkt.din = rw.data;
 //   $display($time,"==========adapter reg2bus  write = %h ===========",rw.kind);   //DEBUG PURPOSE ONLY
 //   $display($time,"==========adapter reg2bus din = %h ===========",rw.data);      //DEBUG PURPOSE ONLY
 //   $display($time,"==========adapter reg2bus addr = %h ===========",rw.addr);     //DEBUG PURPOSE ONLY
   return pkt;  
   endfunction: reg2bus
 
   virtual function void bus2reg( uvm_sequence_item bus_item, ref uvm_reg_bus_op rw );
      packet pkt;
 
      if ( ! $cast( pkt, bus_item ) ) begin
         `uvm_fatal( get_name(), "bus_item is not of the packet type." )
         return;
      end
 
      rw.kind = ( pkt.write == 0) ? UVM_READ : UVM_WRITE; 
      if(rw.kind == UVM_WRITE) 
      rw.data = pkt.din;
      else
      rw.data = pkt.dout;
      rw.addr = pkt.addr;
  //  $display($time,"==========adapter bus2reg  write = %h ===========",rw.kind); //DEBUG PURPOSE ONLY
  //  $display($time,"==========adapter bus2reg data = %h ===========",rw.data);  //DEBUG PURPOSE ONLY  
  //  $display($time,"==========adapter bus2reg addr = %h ===========",rw.addr);  //DEBUG PURPOSE ONLY  
  endfunction
  
endclass: adapter

