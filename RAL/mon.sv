
class mon extends uvm_monitor ;
`uvm_component_utils(mon)
uvm_analysis_port #(packet) an_port;

 virtual intf vif;
 packet pkt;
  function new(string name, uvm_component parent);
      super.new(name, parent);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new


virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
    an_port = new("an_port", this);
   uvm_config_db#(virtual intf)::get(this,"","vif",vif);
endfunction


virtual task run_phase(uvm_phase phase);
  forever begin
    pkt = packet::type_id::create("pkt");
   @( vif.mon2)
    if(vif.mon2.write)begin
     pkt.write =vif.mon2.write;
     pkt.din = vif.mon2.din;
     pkt.addr = vif.mon2.addr;
//    $display($time,"====== mon  vif.write = %h =======", vif.write);
//    $display($time,"====== mon  vif.din = %h =======",   vif.din);
//    $display($time,"====== mon  vif.addr = %h =======",  vif.addr);
   an_port.write(pkt);
   end

    else if(!vif.mon2.write)begin
     pkt.write=vif.mon2.write;
     pkt.dout = vif.mon2.dout;
     pkt.addr = vif.mon2.addr;
//    $display($time,"====== mon  vif.write = %h =======", vif.write);
//    $display($time,"====== mon  vif.dout = %h =======",   vif.dout);
//    $display($time,"====== mon  vif.addr = %h =======",  vif.addr);
   an_port.write(pkt);
   end

 end
endtask

endclass


