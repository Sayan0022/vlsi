

class packet extends uvm_sequence_item;

rand bit [7:0]din;
bit [7:0]dout;
rand bit [7:0]addr;
rand bit write;

constraint a1{din<255; din>=0;}
constraint addr1{addr inside {0,1};}

`uvm_object_utils_begin(packet)
  `uvm_field_int(din, UVM_ALL_ON)
  `uvm_field_int(addr,UVM_ALL_ON )
  `uvm_field_int(write,UVM_ALL_ON )
  `uvm_field_int(dout,UVM_ALL_ON )
`uvm_object_utils_end

  function new(string name = "packet");
      super.new(name);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new

endclass

