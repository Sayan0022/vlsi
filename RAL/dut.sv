module add1(din,dout,clk,rst,write,addr);
input clk,rst,write;
input [7:0]din;
input [7:0]addr;
output reg [7:0]dout;
reg [7:0]rg1;
reg [7:0]rg2;

always @ (posedge clk or negedge rst)
 if(!rst) begin
   rg1<=0;
 end
  else if(write && addr ==0) begin
   rg1<=din;
  end

always @ (posedge clk or negedge rst)
 if(!rst) begin
   rg2<=0;  
 end
  else if(write && addr ==1) begin
   rg2<=din;
  end

always @ *
if(!write && addr == 0)
dout <= rg1;
else if(!write && addr == 1)
dout <= rg2;


endmodule

