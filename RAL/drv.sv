//RAL Driver

class drv extends uvm_driver #(packet);
`uvm_component_utils(drv)

 virtual intf vif;
 
  function new(string name, uvm_component parent);
      super.new(name, parent);

  endfunction


virtual function void build_phase (uvm_phase phase);
    super.build_phase(phase);
   uvm_config_db#(virtual intf)::get(this,"","vif",vif);
endfunction

virtual function void end_of_elaboration_phase (uvm_phase phase);
    uvm_top.print_topology();
endfunction

virtual task run_phase(uvm_phase phase);
 packet req;
 forever begin
  seq_item_port.get_next_item(req);
   @(vif.drv2)
     vif.drv2.write <= req.write;
     vif.drv2.din <= req.din;
     vif.drv2.addr <= req.addr;
   @(vif.drv2)
     if(!req.write)
     req.dout = vif.dout;
//    $display($time,"====== drv  vif.write = %h =======", vif.write); //DEBUG PURPOSE ONLY 
//    $display($time,"====== drv  vif.din = %h =======",   vif.din);   //DEBUG PURPOSE ONLY 
//    $display($time,"====== drv  vif.addr = %h =======",  vif.addr);  //DEBUG PURPOSE ONLY 
//    $display($time,"====== drv  vif.dout = %h =======",  vif.dout);  //DEBUG PURPOSE ONLY 
//    $display($time,"====== drv  pkt.dout = %h =======",  req.dout);  //DEBUG PURPOSE ONLY  
  seq_item_port.item_done();
 end
endtask

endclass

