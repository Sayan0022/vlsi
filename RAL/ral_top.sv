
`include "uvm_macros.svh"
import uvm_pkg::*;

`include "packet.sv"
`include "reg_block.sv"

`include "packet_reg_sequence.sv"
`include "adapter.sv"
`include "interface.sv"

`include "dut.sv"
`include "drv.sv"
`include "mon.sv"
`include "agent.sv"
`include "env.sv"
`include "test.sv"
`include "tb.sv"
`include "top.sv"
