
//`include "packet.sv"
//`include "reg_block.sv"


class packet_sequence extends uvm_sequence #(packet);
 `uvm_object_utils(packet_sequence)
   packet seq;

  function new(string name="" );
      super.new(name);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH) ;
  endfunction: new
  
 virtual task body();

   if(starting_phase != null)
    starting_phase.raise_objection(this);

 repeat(8) begin
   req = packet::type_id::create("req");
   start_item(req);
     if(!req.randomize())
       `uvm_error("TRACE",$sformatf("%m")); 
   finish_item(req);
   req.print();
 end

   if(starting_phase != null)
    starting_phase.drop_objection(this);
 endtask

endclass

 //typedef uvm_sequencer #(packet) m_sequencer;

class packet_reg_sequence extends uvm_reg_sequence;
 `uvm_object_utils(packet_reg_sequence)
  packet req;
  reg_block regmodel;
  uvm_status_e status;
  int read_val;
  int des_val;
  int mirror_val;
  
  function new(string name = "" );
      super.new(name);
      `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH);
  endfunction: new
  
 virtual task body();
  uvm_config_db#(reg_block)::get(m_sequencer,"*","regmodel",regmodel);
 

   req = packet::type_id::create("req");
   if(starting_phase != null)
    starting_phase.raise_objection(this, get_type_name());

    write_reg(regmodel.rg_1,status,8'h81);
     des_val = regmodel.rg_1.get();
         mirror_val = regmodel.rg_1.get_mirrored_value();
    $display($time,"============================= des_val = %h =============================",des_val);
    $display($time,"============================= mirror_val = %h =============================",mirror_val);
    read_reg(regmodel.rg_1,status,read_val);
     $display($time,"============================= read_val = %h =============================",read_val);
    regmodel.rg_1.set(8'h56);
    des_val = regmodel.rg_1.get(); 
    $display($time,"============================= des_val = %h =============================",des_val);
    regmodel.rg_1.update(status);
    read_reg(regmodel.rg_1,status,read_val);
     $display($time,"============================= read_val = %h =============================",read_val);
    regmodel.rg_1.predict(8'h39);
    des_val = regmodel.rg_1.get(); 
    $display($time,"============================= des_val = %h =============================",des_val);
    write_reg(regmodel.rg_2,status,8'ha7);
    read_reg(regmodel.rg_2,status,read_val);
     $display($time,"============================= read_val = %h =============================",read_val);
   
   if(starting_phase != null)
    starting_phase.drop_objection(this,get_type_name());
 endtask



endclass


