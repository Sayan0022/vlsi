module apb_slave #(parameter addr_width=32,parameter data_width=32)
(
input clk,reset_n,
input [addr_width-1:0]paddr,[data_width-1:0]pwdata,
input pwrite,pselx,penable,ptransfer=0,
output reg pready=0,
output reg [data_width-1:0]prdata=0
);
reg[2:0]state;
reg[2:0]next_state;

parameter [2:0] IDLE=2'b00;
parameter [2:0] SETUP=2'b01;
parameter [2:0] ACCESS=2'b10;

always @(posedge clk, posedge reset_n)
	begin
	if (reset_n)
   		state=IDLE;
  	else
   		state=next_state;
        end

always @(state, next_state)
	begin
	case(state)
	IDLE:
	    if(penable ==0 && pready ==0)  
   		begin
			if (pselx==1)
			begin
    			next_state=SETUP;
    			pready<=0;
			prdata<=0;
   			end
			else begin
			if(ptransfer==0)	
			begin
			next_state=IDLE;
    			pready<=0;
			prdata<=0;
			end
		end
		end
   	    
	SETUP:

         	if(pselx ==1 && pready==0)  
		begin
			if (penable==1)
			begin
    			next_state=ACCESS;
	  		pready<=0;
	 		prdata<=0;
   			end
		end
  	ACCESS:
   		if(pselx ==1 && penable ==1 )  
		begin
			if (pready==0)
			begin
    			next_state=ACCESS;
	  		pready<=0;
	 		prdata<=0;
			end
			else
			begin	
			if (ptransfer==0)
			begin
			next_state=IDLE;
			pready<=1;
	 		prdata<=pwdata;
   			end
			else 
			begin
			next_state=SETUP;
			pready<=0;
	 		prdata<=0;
			end
			end
		end
  	default:
   		begin
        		next_state=IDLE;
			pready<=0;
	 		prdata<=0;

           		
   		end
  		endcase
 	end
endmodule

