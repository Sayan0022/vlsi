`include "uvm_macros.svh"
import uvm_pkg::*;
//package all_files;
//`include "apb_design.sv"
`include "apb_dutt.sv"
`include "apb_interface.sv"
`include "apb_seq_items.sv"
`include "apb_sequence.sv"
`include "apb_sequencer.sv"
`include "apb_driver.sv"
`include "apb_monitor.sv"
`include "apb_agent.sv"
`include "apb_scoreboard.sv"
`include "apb_env.sv"
`include "apb_test.sv"
`include "apb_wr_rd_test.sv"
`include "apb_tb_top.sv"



//`include "dut.sv"
//endpackage
