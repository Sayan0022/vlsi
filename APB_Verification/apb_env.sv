//apb_env

//`include "apb_scoreboard.sv"
//`include "apb_agent.sv"
//`include "uvm_macros.svh"
//import uvm_pkg::*;

//factory registration
class apb_env extends uvm_env;
`uvm_component_utils(apb_env)

//factory registration
function new(string name,uvm_component parent);
super.new(name,parent);
endfunction

//instantiation
apb_scoreboard scoreboard;
apb_agent agent;

//build_phase
function void build_phase(uvm_phase phase);
super.build_phase(phase);
agent =apb_agent::type_id::create("agent", this);
scoreboard = apb_scoreboard::type_id::create("scoreboard", this);
endfunction

//connect_phase
function void connect_phase(uvm_phase phase);
agent.apb_mo.item_collected_port.connect(scoreboard.item_collected_export);
endfunction

endclass
