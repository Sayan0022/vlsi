//apb_test
//`include "uvm_macros.svh"
//import uvm_pkg::*;

//`include "apb_env.sv"

class apb_test extends uvm_test;
`uvm_component_utils(apb_test)

function new(string name,uvm_component parent);
super.new(name,parent);
endfunction

//instantion
apb_env env;

//build_phase
function void build_phase(uvm_phase phase);
super.build_phase(phase);

env=apb_env::type_id::create("env",this);
endfunction

//end_of_elaboration

function void report_phase(uvm_phase phase);
uvm_report_server svr;
super.report_phase(phase);

svr=uvm_report_server::get_server();
if(svr.get_severity_count (UVM_FATAL)+svr.get_severity_count(UVM_ERROR)>0)begin
`uvm_info(get_type_name(),"...........Test Pass................",UVM_NONE)
end
else begin
`uvm_info(get_type_name(),"...........test fail................",UVM_NONE)
end
endfunction
endclass
