//apb_agent

//`include "uvm_macros.svh"
//import uvm_pkg::*;

//`include "apb_seq_items.sv"
//`include "apb_sequence.sv"
//`include "apb_sequencer.sv"
//`include "apb_driver.sv"
//`include "apb_monitor.sv"
//factory registration
class apb_agent extends uvm_agent;
`uvm_component_utils(apb_agent)

//constructor
function new(string name,uvm_component parent);
super.new(name,parent);
endfunction

//component handles
apb_sequencer apb_sqr;
apb_driver apb_dv;
apb_monitor apb_mo;

//build phase-creating monitor,driver and sequencer
function void build_phase(uvm_phase phase);
super.build_phase(phase);
apb_mo=apb_monitor::type_id::create("apb_mo",this);

if(get_is_active() == UVM_ACTIVE)
begin
apb_dv=apb_driver::type_id::create("apb_dv",this);
apb_sqr=apb_sequencer::type_id::create("apb_sqr",this);
end
endfunction

//connect_phase-connecting sequencer to driver
function void connect_phase(uvm_phase phase);
if(get_is_active()==UVM_ACTIVE)begin
apb_dv.seq_item_port.connect(apb_sqr.seq_item_export);//Tlm port
end
endfunction
endclass
