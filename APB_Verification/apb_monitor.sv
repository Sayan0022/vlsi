//apb_monitor
//factory registration

//`include "uvm_macros.svh"
//import uvm_pkg::*;
//`include "apb_seq_items.sv"
//`include "apb_monitor.sv"
class apb_monitor extends uvm_monitor;
`uvm_component_utils(apb_monitor)

//virtual interface
virtual apb_intf vif;

//this property hols thetransaction collected items
apb_seq_items trans_collected;

//analysis port to send the transaction to score board
uvm_analysis_port#(apb_seq_items) item_collected_port;

covergroup cover_group ;
c1: coverpoint vif.monitor_cb.paddr{bins b1 ={[67108863:268435455]}; }
c2: coverpoint vif.monitor_cb.pselx{bins b2 ={[0:1]}; }
c3: coverpoint vif.monitor_cb.penable;
c4: coverpoint vif.monitor_cb.pwrite;
//c5: coverpoint vif.monitor_cb.pwdata{bins b3 ={[0:4227858431]}; }
c6: coverpoint vif.monitor_cb.prdata{bins b4 = {[0:4294967295]}; }
c7: coverpoint vif.monitor_cb.pready{bins b5 = {[0:1]};}
endgroup
//
//constructor
function new(string name,uvm_component parent);
super.new(name,parent);
trans_collected=new();
cover_group =new;
item_collected_port=new("item_collected_port",this);
endfunction

function asseration();
assert(vif.monitor_cb.pselx == 1) $display ("OK");
else $error("It's gone wrong");
assert(vif.clk == vif.monitor_cb.penable) $display("Correct");
else $error("wrong");

endfunction


//build_phase-getting the interface handle
function void build_phase(uvm_phase phase);
super.build_phase(phase);
if(!uvm_config_db#(virtual apb_intf)::get(this,"","vif",vif))
`uvm_fatal("NOVIF",{"virtual interface must be set for:",get_full_name(),".vif"});
endfunction





//run_phase-convert the pin wiggle to transaction level
virtual task run_phase(uvm_phase phase);


forever begin
@(posedge vif.clk);
trans_collected.paddr =vif.monitor_cb.paddr;
trans_collected.pselx= vif.monitor_cb.pselx;
trans_collected.penable= vif.monitor_cb.penable;
//trans_collected.pready= vif.monitor_cb.pready;
if(vif.monitor_cb.pwrite)begin//write operation
trans_collected.pwrite= vif.monitor_cb.pwrite;
trans_collected.pwdata= vif.monitor_cb.pwdata;
trans_collected.prdata= vif.monitor_cb.prdata;
$display("NITESH::%d",vif.paddr);
end
cover_group.sample();
asseration();

item_collected_port.write(trans_collected);
//else begin//read operation
//trans_collected.prdata= vif.monitor_cb.prdata;
//end 
end
endtask
endclass
