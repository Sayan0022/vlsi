//apb_driver

`define DRIV_IF apb_vif.driver_cb
//`include "uvm_macros.svh"
//`include "apb_sequencer.sv"
//import uvm_pkg::*;

//factory registration
class apb_driver extends uvm_driver#(apb_seq_items);
`uvm_component_utils(apb_driver)

//constructor
function new(string name,uvm_component parent);
super.new(name,parent);
endfunction

//virtual interface

virtual apb_intf apb_vif;

//build phase
function void build_phase(uvm_phase phase);
super.build_phase(phase);
if(!uvm_config_db#(virtual apb_intf)::get(this,"","vif",apb_vif))
`uvm_fatal("NO_VIF",{"virtual interface must be set for:",get_full_name(),".vif"});
endfunction

//run phase to drive the seq_item to DUT

task run_phase(uvm_phase phase);
forever begin
seq_item_port.get_next_item(req);
drive();
seq_item_port.item_done();
end
endtask

//driver logic
virtual task drive();

`DRIV_IF.paddr <=req.paddr;
`DRIV_IF.pselx <=req.pselx;
`DRIV_IF.penable <=1;

if (req.pwrite)begin//write operation

`DRIV_IF.pwrite <=req.pwrite;
`DRIV_IF.pwdata <=req.pwdata;

@(posedge apb_vif.clk);

`DRIV_IF.penable <=req.penable;

@(posedge apb_vif.clk);
`DRIV_IF.penable <=1;
`DRIV_IF.pselx <=1;
@(posedge apb_vif.clk);
`DRIV_IF.penable <=1;

end


else if(req.pwrite==0)begin//read operation
`DRIV_IF.pwrite <=req.pwrite;
req.prdata=`DRIV_IF.prdata;

@(posedge apb_vif.clk);

`DRIV_IF.penable <=req.penable;

@(posedge apb_vif.clk);
`DRIV_IF.penable <=1;
`DRIV_IF.pselx <=1;
@(posedge apb_vif.clk);
`DRIV_IF.penable <=1;


end

endtask
endclass
