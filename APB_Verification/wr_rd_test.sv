//wr_rd test
`include "uvm_macros.svh"
`include "apb_test.sv"
import uvm_pkg::*;
class wr_rd_test extends apb_test;
`uvm_component_utils(wr_rd_test)

function new(string name,uvm_component parent);
super.new(name,parent);
endfunction

//creating handles
write_sequence wr_seq;
read_sequence rd_seq;

//build_phase-create the sequence
function void build_phase(uvm_phase phase);
super.build_phase(phase);

wr_seq=write_sequence::type_id::create("wr_seq",this);
rd_seq=read_sequence::type_id::create("rd_seq",this);
endfunction

//run_phase-starting the test

task run_phase(uvm_phase phase);
phase.raise_objection(this);
	seq.start(env.agent.apb_seq);
phase.drop_objection(this);

//set a drain time for the environment
phase.phase_done.set_drain_time(this,50);
endtask
endclass
