class APB_Coverage;
  
  APB_Transactor_c pkt;
  
  covergroup cg;
    coverpoint pkt.PWRITE{
      bins pwr_1 = (1=>0);
    }
    coverpoint pkt.PADDR{
      bins paddr_1[16] = {[0:$]};
    }
    coverpoint pkt.PSEL{
      bins psel_1[] = {0,1};
    }
    coverpoint pkt.PENABLE{
      bins pen_1[] = {0,1};
    }
  endgroup
  
  function new();
    cg = new();
  endfunction
  
  task sample(APB_Transactor_c pkt);
    this.pkt = pkt;
    cg.sample();
    $display("coverage completed = %2f%%",cg.get_coverage());
  endtask
  
endclass

