//`include"APB_Generator.svh"
/*`include"APB_Driver.svh"
`include"APB_Monitor_1.svh"
`include"APB_Monitor_2.svh"
`include"APB_Scoreboard.svh"
`include "ref_logic.svh"*/

class APB_Enviroment_c;
  APB_Driver_c driver;
  APB_Generator_c generator;
  APB_Monitor_1 monitor1;
  APB_Monitor_2 monitor2;
  ref_dut refer;
  APB_Scoreboard_c scoreboard;
  mailbox gen2drive;
  mailbox mon12sb;
  mailbox mon22ref;
  mailbox ref2sb;
  virtual APB_Interface_c vintf;

  function new(virtual APB_Interface_c vintf);
    this.vintf = vintf;
    gen2drive = new();
    mon12sb = new();
    ref2sb = new();
    mon22ref = new();
    generator = new(gen2drive);
    driver = new(vintf,gen2drive);
    monitor1 = new(vintf,mon12sb);
    monitor2 = new(vintf,mon22ref);
    refer = new(vintf,mon22ref,ref2sb);
    scoreboard = new(mon12sb,ref2sb);
  endfunction

	task pre_test();
      $display("-------------Enter in PRE--------------");
      driver.reset();
      wait(vintf.PRESET);
      if(vintf.PRESET)
        $display("------------- Reset is Done --------------");
	endtask

	task test();
		fork
          $display("-------------Enter in MAIN--------------");
          generator.main();
          driver.main();
          monitor1.main();
          monitor2.main();
          refer.main();
          scoreboard.main();
        join
	endtask

	task post_test();
      $display("-------------Enter in POST--------------");
      wait(generator.end_event.triggered);
      if(driver.count == generator.loop)
        $display("-----All test cases applied SUCCESSFULLY-----");
      else 
        $display("-----All test cases applied UNSUCCESSFULLY-----");
	endtask

	task main();
		pre_test();
		test();
		post_test();
	endtask
  
endclass


