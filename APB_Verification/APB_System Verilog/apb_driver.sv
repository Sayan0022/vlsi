class APB_Driver_c;
	virtual APB_Interface_c vintrf;
	APB_Transactor_c trans;
	int count,ready_count;
  	mailbox gen2drive;
	
  function new(virtual APB_Interface_c vintrf,mailbox gen2drive);
		this.vintrf = vintrf;
		this.gen2drive = gen2drive;
	endfunction

	task reset();
		wait(!vintrf.PRESET);
		$display("-------------Reset Started---------------");
		vintrf.DRIVER.driver_cb.PADDR <= 0;
		vintrf.DRIVER.driver_cb.PWDATA <= 0;
		vintrf.DRIVER.driver_cb.PENABLE <= 0;
		vintrf.DRIVER.driver_cb.PSEL <= 0;
		vintrf.DRIVER.driver_cb.PWRITE <= 0;
      	ready_count = 0;
      	count = 0;
		wait(vintrf.PRESET);
		if(!vintrf.PRESET)
          $display("-----------Reset Not Completed-----------");
        else
          $display("-------------Reset Completed--------------");
	endtask
  
  task write_t(int en_val);
    ready_count = 0;
    $display("-------------Write Operation Started---------------");
    vintrf.DRIVER.driver_cb.PWRITE <= 1;
    vintrf.DRIVER.driver_cb.PSEL <= 1;
    vintrf.DRIVER.driver_cb.PENABLE <= 0;
    vintrf.DRIVER.driver_cb.PADDR <= trans.PADDR;
    vintrf.DRIVER.driver_cb.PWDATA <= trans.PWDATA;
    for(int i=0;i<en_val;i++)
      @(posedge vintrf.PCLK);
    vintrf.DRIVER.driver_cb.PENABLE <= 1;
    trans.PREADY = vintrf.DRIVER.driver_cb.PREADY;
    while(!trans.PREADY)
      begin
        ready_count++;
        if(ready_count==15)
          begin
            $display("-------------Write Operation Terminated---------------");
            ready_count=0;
            disable write_t;
            break;
          end
      end 
    @(posedge vintrf.PCLK);
    vintrf.DRIVER.driver_cb.PENABLE <= 0;
    vintrf.DRIVER.driver_cb.PSEL <= 0;
  endtask
 
  task read_t(int en_val);
    ready_count = 0;
    $display("-------------Read Operation Started---------------");
    vintrf.DRIVER.driver_cb.PWRITE <= 0;
    vintrf.DRIVER.driver_cb.PSEL <= 1;
    vintrf.DRIVER.driver_cb.PENABLE <= 0;
    trans.PREADY = vintrf.DRIVER.driver_cb.PREADY;
    if(trans.PREADY) begin
      vintrf.DRIVER.driver_cb.PADDR <= trans.PADDR;
      for(int i=0;i<en_val;i++)
        @(posedge vintrf.PCLK);
      vintrf.DRIVER.driver_cb.PENABLE <= 1;
      @(posedge vintrf.PCLK);
      vintrf.DRIVER.driver_cb.PENABLE <= 0;
      vintrf.DRIVER.driver_cb.PSEL <= 0;
      end
      else begin
        while(!trans.PREADY)
        begin
          ready_count++;
          if(ready_count==15)
            begin
              $display("-------------Read Operation Terminated---------------");
              ready_count=0;
              disable read_t;
              break;
            end
        end 
      end
  endtask
 
	task main();
		forever 
          begin
			gen2drive.get(trans);
            
            case(trans.states)
              err: begin // Protocol Violation
                write_t(2);
                read_t(2);
              end
              tc1: begin
                write_t(1);
                read_t(1);
              end
              tc2: read_t(1);
              tc3: write_t(1);
              default: write_t(1);
            endcase
          end 
	endtask
  
endclass


