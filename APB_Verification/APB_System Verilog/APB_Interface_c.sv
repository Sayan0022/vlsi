interface APB_Interface_c(input logic PCLK,PRESET);
  logic [4:0] PADDR;
  logic [7:0] PWDATA;
  logic PSEL;
  logic PENABLE;
  logic PWRITE;
  logic [7:0] PRDATA;
  logic PREADY;

	clocking driver_cb @(posedge PCLK);
		default input #2 output #2;
		output PADDR;
		output PWDATA;
		output PSEL;
		output PENABLE;
        output PWRITE;
		input PRDATA;
		input PREADY;
	endclocking

	clocking monitor_cb @(posedge PCLK);
		default input #2 output #2;
		input PADDR;
		input PWDATA;
		input PSEL;
      	input PENABLE;
      	input PWRITE;
		input PRDATA;
		input PREADY;
	endclocking

	modport DRIVER(clocking driver_cb, input PCLK,PRESET);
	modport MONITOR(clocking monitor_cb, input PCLK,PRESET);
endinterface


