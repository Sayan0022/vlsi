`include "uvm_macros.svh"
import uvm_pkg::*;


`include "cfg.sv"

`include "apb_transactor.sv"
`include "apb_generator.sv"
`include "apb_driver.sv"
`include "apb_monitor_1.sv"
`include "apb_monitor_2.sv"
`include "APB_Interface_c.sv"
`include "apb_coverage.sv"
`include "apb_scoreboard.sv"
`include "apb_ref_logic.sv"

`include "apb_env.sv"
`include "apb_test.sv"

`include "apb_design.sv"
`include "apb_tb_top.sv"
`include "apb_tb_top.sv"



