typedef enum int {s1,s2,s3} state;
class ref_dut;
  virtual APB_Interface_c vintf;
  state current_state, next_state;
  APB_Transactor_c trans;
  mailbox mon22ref;
  mailbox ref2sb;
  int count;
  int memory[*];
  
  function new(virtual APB_Interface_c vintf, mailbox mon22ref, mailbox ref2sb);
    this.vintf = vintf;
    this.mon22ref = mon22ref;
    this.ref2sb = ref2sb;
  endfunction
  
  task main();
    begin
      $display("----------- Ref Logic Main ! ------------");
      current_state = s1;
      forever
        begin
          mon22ref.get(trans); 
          case(current_state)
            s1: begin
              if(trans.PSEL && !trans.PENABLE && count==0)
                begin
                  current_state = s2;
                  trans.PREADY = 1;
                end
              else
                begin
                  current_state = s1;
                  trans.PREADY = 1;
                end
            end

            s2: begin
              if(trans.PSEL && trans.PENABLE)
                begin
                  current_state = s3;
                end
              else if(trans.PSEL && !trans.PENABLE)
                begin
                  count=count+1;
                  if(count>=1)
                    current_state = s1;
                  else
                    current_state = s2;
                 end
              else
                begin
                  current_state = s1;
                end
              
              trans.PREADY = 1;
            end

            s3: begin
              if(trans.PSEL && !trans.PENABLE)
                begin
                  current_state = s2;
                  if(trans.PWRITE)
                    memory[trans.PADDR] = trans.PWDATA;
                  else if(!trans.PWRITE)
                    trans.PRDATA = memory[trans.PADDR];
                  else
                    trans.PRDATA = 8'hAA;
                end
              else
                begin
                  current_state = s1;
                end
              trans.PREADY = 1;
            end
            
            default:begin
              current_state = s1;
              trans.PRDATA = 1;
            end
          endcase
          ref2sb.put(trans);
        end
    end
  endtask
endclass

