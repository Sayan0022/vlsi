
`include "APB_Interface.svh"
`include "APB_Test.svh"
module APB_Tb_top();
	bit PCLK;
	bit PRESET;
    parameter reset_cycle = 2;
	initial PCLK=1'b0;
	always #20 PCLK=~PCLK;

	initial 
      begin
		PRESET = 0; 
        for(int i=0; i<=reset_cycle; i++)
        @(posedge vintrf.PCLK);
		PRESET = 1;
        #5000;
        #3000;
        $finish(2000);
      end
 
	APB_Interface_c vintrf(PCLK,PRESET);
  
  	APB_Slave_DUT DUT(.PADDR(vintrf.DRIVER.driver_cb.PADDR), .PWDATA(vintrf.DRIVER.driver_cb.PWDATA), .PRDATA(vintrf.PRDATA), .PWRITE(vintrf.DRIVER.driver_cb.PWRITE), .PENABLE(vintrf.DRIVER.driver_cb.PENABLE), .PSEL(vintrf.DRIVER.driver_cb.PSEL), .PREADY(vintrf.PREADY), .PRESET(vintrf.PRESET), .PCLK(vintrf.PCLK));
  
  	APB_Test_c test(vintrf);
	
	initial begin
      $dumpfile("APB_Tb_top.vcd");
      $dumpvars(0,APB_Tb_top);
	end
endmodule

