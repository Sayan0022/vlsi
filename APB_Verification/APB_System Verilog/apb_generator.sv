//`include"APB_Transactor.svh"
//`include "cfg.svh"
class APB_Generator_c extends cfg;
  rand APB_Transactor_c trans;
  int loop;
  mailbox gen2drive;
  event end_event;
  int seq_number = 5;

  function new(mailbox gen2drive);
    this.gen2drive = gen2drive;
  endfunction

	task main();
      while(seq_number >= 1)
        begin
          seq_name(seq_number);
          case(cfg::testname)
            "Error_Injection_WR": begin
              $display("%s",cfg::testname);
              repeat(loop) begin
          		trans=new();
                if(!trans.randomize() with{states == err;})
                  $display("-----Randomization Error!------");
                else
                  $display("-----Randomization Success!------");
                gen2drive.put(trans);
              end
            end
            
            "Write_Read": begin
              $display("%s",cfg::testname);
              repeat(loop) begin
                trans=new();
                if(!trans.randomize() with{states == tc1;})
                  $display("-----Randomization Error!------");
                else
                  $display("-----Randomization Success!------");
                gen2drive.put(trans);
              end
            end
            
            "READ": begin
            $display("%s",cfg::testname);
              repeat(loop) begin
          		trans=new();
                if(!trans.randomize() with{states == tc2;})
                  $display("-----Randomization Error!------");
                else
                  $display("-----Randomization Success!------");
                gen2drive.put(trans);
              end
            end
            
            "WRITE": begin
            $display("%s",cfg::testname);
              repeat(loop) begin
                trans=new();
                if(!trans.randomize() with{states == tc3;})
                  $display("-----Randomization Error!------");
                else
                  $display("-----Randomization Success!------");
                gen2drive.put(trans);
              end
            end
            
            "DEFAULT_SEQUENCE": begin
            $display("%s",cfg::testname);
              repeat(loop) begin
                trans=new();
                if(!trans.randomize() with{states == def;})
                  $display("-----Randomization Error!------");
                else
                  $display("-----Randomization Success!------");
                gen2drive.put(trans);
              end
            end
        endcase
          seq_number--;
        end
      -> end_event;
            
    endtask
endclass


