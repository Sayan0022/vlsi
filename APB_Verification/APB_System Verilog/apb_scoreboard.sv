//`include "APB_Coverage.svh"
typedef enum {PASS, FAIL} e_true_false;  
class APB_Scoreboard_c;
  APB_Transactor_c trans_1,trans_2;
  mailbox mon12sb;
  mailbox ref2sb;
  int loop;
  APB_Coverage cov = new();
  
  function new(mailbox mon12sb, mailbox ref2sb);
    this.mon12sb = mon12sb;
    this.ref2sb = ref2sb;
  endfunction
  
  task main();
    e_true_false cond;
    $display("----------- Scoreboard Main ! ------------");
    mon12sb.get(trans_1);
    forever begin
      ref2sb.get(trans_2);
      ref2sb.get(trans_2);
      mon12sb.get(trans_1);
      if((trans_1.PADDR == trans_2.PADDR) && (trans_1.PWDATA == trans_2.PWDATA) && (trans_1.PRDATA == trans_2.PRDATA))
        begin
          cond = PASS;
          $display("DUT_addr:%h, DUT_wdata:%h, DUT_rdata:%h, Checker_addr:%h, Checker_wdata:%h, Checker_rdata:%h, Test:%s", trans_1.PADDR, trans_1.PWDATA, trans_1.PRDATA, trans_2.PADDR, trans_2.PWDATA, trans_2.PRDATA, cond.name);
          cov.sample(trans_2);
        end
      else
        begin
          cond = FAIL;
          $display("DUT_addr:%h, DUT_wdata:%h, DUT_rdata:%h, Checker_addr:%h, Checker_wdata:%h, Checker_rdata:%h, Test:%s", trans_1.PADDR, trans_1.PWDATA, trans_1.PRDATA, trans_2.PADDR, trans_2.PWDATA, trans_2.PRDATA, cond.name);
        end
    end
  endtask
endclass

