class APB_Monitor_1;
  APB_Transactor_c trans;
  mailbox mon2sb;
  virtual APB_Interface_c vintf;
  
  function new(virtual APB_Interface_c vintf, mailbox mon2sb);
    this.vintf = vintf;
    this.mon2sb = mon2sb;
  endfunction
  
  task main();
    forever begin
      trans = new();
      @(posedge vintf.PCLK);
      trans.PADDR = vintf.MONITOR.monitor_cb.PADDR;
      trans.PWRITE = vintf.MONITOR.monitor_cb.PWRITE;
      trans.PSEL = vintf.MONITOR.monitor_cb.PSEL;
      trans.PREADY = vintf.MONITOR.monitor_cb.PREADY;
      trans.PWDATA = vintf.MONITOR.monitor_cb.PWDATA;
      @(posedge vintf.PCLK);
      trans.PENABLE = vintf.MONITOR.monitor_cb.PENABLE;
      trans.PRDATA = vintf.MONITOR.monitor_cb.PRDATA;
      mon2sb.put(trans);
    end
  endtask
  
endclass

