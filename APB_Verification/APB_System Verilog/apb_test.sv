//`include "APB_Enviroment.svh"
program APB_Test_c(APB_Interface_c vintf);
  int no_transition = 10;
  APB_Enviroment_c env;
  
	initial begin
      env = new(vintf);
      env.generator.loop = no_transition;
      env.scoreboard.loop = no_transition;
	  env.main();
	end
endprogram	


