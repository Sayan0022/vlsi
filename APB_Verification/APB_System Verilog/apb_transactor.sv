typedef enum{tc1=1,tc2,tc3,err,def} t_cases;
class APB_Transactor_c;
  rand t_cases states;
  rand bit [4:0] PADDR;
  rand bit [7:0] PWDATA;
  rand bit PWRITE;
  rand bit PSEL;
  rand bit PENABLE;
  bit [7:0] PRDATA;
  bit PREADY;
  
  constraint Add_const{PADDR < 32;}
  constraint Data_const{PWDATA < 256;}
  
endclass

