class APB_Monitor_2;
  APB_Transactor_c trans;
  mailbox mon22ref;
  virtual APB_Interface_c vintf;
  
  function new(virtual APB_Interface_c vintf, mailbox mon22ref);
    this.vintf = vintf;
    this.mon22ref = mon22ref;
  endfunction
  
  task main();
    forever begin
      trans = new();
      @(posedge vintf.PCLK);
      trans.PADDR = vintf.MONITOR.monitor_cb.PADDR;
      trans.PWRITE = vintf.MONITOR.monitor_cb.PWRITE;
      trans.PSEL = vintf.MONITOR.monitor_cb.PSEL;
      trans.PREADY = vintf.MONITOR.monitor_cb.PREADY;
      trans.PWDATA = vintf.MONITOR.monitor_cb.PWDATA;
      trans.PRDATA = vintf.MONITOR.monitor_cb.PRDATA;
      trans.PENABLE = vintf.MONITOR.monitor_cb.PENABLE;
      mon22ref.put(trans);
    end
  endtask
  
endclass

