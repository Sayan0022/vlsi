//apb_tb_top

//include interface and testcases

//`include "apb_test.sv"
//`include "wr_rd_test.sv"
`include "uvm_macros.svh"
import uvm_pkg::*;
//import all_files::*;
//`include "apb_interface.sv"
//`include "apb_test.sv"
//`include "apb_wr_rd_test.sv"

module tb_top;

//declaration clk and reset
bit clk;
bit reset;

//clock generation
always #5 clk= ~clk;

//reset generation

initial begin
reset=1;
#5 reset=0;
end

//interface instance
apb_intf intf(clk,reset);

//DUT instantiation

apb_slave DUT(
.clk(intf.clk),
.reset_n(intf.reset),
.paddr(intf.paddr),
.pwrite(intf.pwrite),
.pselx(intf.pselx),
.penable(intf.penable),
.pwdata(intf.pwdata),
.prdata(intf.prdata),
.pready(intf.pready)
);

//passing the interface handle to the lower hierarchy

initial begin
uvm_config_db#(virtual apb_intf)::set(uvm_root::get(),"*","vif",intf);
$dumpfile("dump.vcd");
$dumpvars;

end

//calling test
initial begin
run_test("apb_wr_rd_test");
end
endmodule
