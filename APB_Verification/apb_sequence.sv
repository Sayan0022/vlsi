//apb sequence
//`include "uvm_macros.svh"
//import uvm_pkg::*;
//`include "apb_seq_items.sv"

//factory registration
class apb_sequence extends uvm_sequence#(apb_seq_items);
`uvm_object_utils(apb_sequence)

//constructor
function new(string name="apb_sequence");
super.new(name);
endfunction

//create and randomize and send the seq_item to driver
virtual task body();
repeat(50) begin
req=apb_seq_items::type_id::create("req");
wait_for_grant();
req.randomize();
req.print();
send_request(req);
wait_for_item_done();
end
endtask
endclass

//write_sequence-no wait state
class write_sequence extends uvm_sequence#(apb_seq_items);
`uvm_object_utils(write_sequence)

//constructor
function new(string name="write_sequence");
super.new(name);
endfunction

virtual task body();
`uvm_do_with(req,{pwrite==1;})
`uvm_do_with(req,{pselx==1;})
//@(posedge clk);
`uvm_do_with(req,{penable==1;})
`uvm_do_with(req,{pready==1;})

//constraint en (req,{pselx==1;}) -> (req,{penable==1;})



//when write enable and penable is 1 and at the same clock cycle it will start writing without wait.
endtask
endclass

//read_sequence-with no wait state
class read_sequence extends uvm_sequence#(apb_seq_items);
`uvm_object_utils(read_sequence)

//constructor
function new(string name="read_sequence");
super.new(name);
endfunction
virtual task body();
`uvm_do_with(req,{pwrite==0;})
`uvm_do_with(req,{pselx==1;})
//@(posedge clk
`uvm_do_with(req,{pready==1;})
`uvm_do_with(req,{penable==1;})

//when pwrite and pready both are enabled but pwrite is waiting for one more clock cyle for penable.
endtask
endclass
