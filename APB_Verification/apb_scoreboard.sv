//apb_scoreboard
//factory registration
`include "uvm_macros.svh"
import uvm_pkg::*;
//`include "apb_seq_items.sv"
//`include "apb_agent.sv"

class apb_scoreboard extends uvm_scoreboard;
`uvm_component_utils(apb_scoreboard)

//constructor
function new(string name,uvm_component parent);
super.new(name,parent);
endfunction


//declare pkt_qu to store thr packet's received from monitor
apb_seq_items pkt_qu[$];

//scoreboard memory
bit[31:0]sc_mem[4];

//ports to receive packets from monitor
uvm_analysis_imp#(apb_seq_items, apb_scoreboard) item_collected_export;

//build_phase-create port and initilize the local memory
function void build_phase(uvm_phase phase);
super.build_phase(phase);
item_collected_export=new("item_collected_export",this);
foreach(sc_mem[i])sc_mem[i] = 8'hFF;
endfunction

//write_task-receives the packet from monitor and pushes to the queue

virtual function void write(apb_seq_items pkt);
pkt_qu.push_back(pkt);
endfunction

//run_phase-compare the read data with expected data which is local memory
//local memory will be updated in the write operation.

virtual task run_phase(uvm_phase phase);
apb_seq_items apb_pkt;

forever begin
wait(pkt_qu.size()>0);
apb_pkt=pkt_qu.pop_front();

if(apb_pkt.pwrite)begin
sc_mem[apb_pkt.paddr]=apb_pkt.pwdata;
`uvm_info(get_type_name(),$sformatf("Write data........................."),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("Addr: %0h",apb_pkt.paddr),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("Data: %0h",apb_pkt.pwdata),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("......................"),UVM_LOW)
end
else if(apb_pkt.pwrite==0) begin
`uvm_info(get_type_name(),$sformatf("Read data......................"),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("Addr: %0h",apb_pkt.paddr),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("Expected data:%0h Actual data: %0h",sc_mem[apb_pkt.paddr],apb_pkt.prdata),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("....................................."),UVM_LOW)
end

else begin
`uvm_error(get_type_name(),$sformatf("Read data mismatch...................."))
`uvm_error(get_type_name(),$sformatf("Addr: %0h",apb_pkt.paddr))
`uvm_info(get_type_name(),$sformatf("Expected data:%0h Actual data: %0h",sc_mem[apb_pkt.paddr],apb_pkt.prdata),UVM_LOW)
`uvm_info(get_type_name(),$sformatf("....................................."),UVM_LOW)
end
end
endtask
endclass
