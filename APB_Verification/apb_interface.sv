
interface apb_intf(input logic clk,reset);
 //declaring signals

logic [31:0]paddr;
logic pselx;
logic penable;
logic pwrite;
logic [31:0]pwdata;
logic [31:0]prdata;
logic pready;

//driver clocking block

clocking driver_cb @(posedge clk);
default input #2 output #2;
output paddr;
output pselx;
output penable;
output pwrite;
output pwdata;
input prdata;
input pready;

endclocking

//monitor clocking block

clocking monitor_cb @(posedge clk);
default input #2 output #2;
input paddr;
input pselx;
input penable;
input pwrite;
input pwdata;
input prdata;
input pready;

endclocking

//Driver modport(optional)

modport DRIVER (clocking driver_cb,input clk,reset);

//Monitor modport(optional)

modport MONITOR(clocking monitor_cb, input clk,reset);

endinterface

//coverage
/*
module axi_cov;
logic [31:0]paddr;
logic pselx;
logic penable;
logic pwrite;
logic [31:0]pwdata;
logic [31:0]prdata;
logic pready;

covergroup cg@(posedge clk;

c1: coverpoint paddr;
c2: coverpoint pselx;
c3: coverpoint penable;
c4: coverpoint pwrite;
c5: coverpoint pwdata;
c6: coverpoint prdata;
c7: coverpoint pready;

endgroup

function cov();
cg cover_inst=new();
endfunction

initial begin
cov();
end
endmodule
*/
