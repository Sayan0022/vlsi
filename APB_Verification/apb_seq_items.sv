//apb-sequence-item
//factory registration

//`include "uvm_macros.svh"
//import uvm_pkg::*;
class apb_seq_items extends uvm_sequence_item;

//input and output
rand bit [31:0]paddr;
     bit pselx=1;
     bit penable;
rand bit pwrite;
rand bit [31:0]pwdata;
     bit [31:0]prdata;
     bit pready=1;


`uvm_object_utils_begin(apb_seq_items)
`uvm_field_int(paddr,UVM_ALL_ON)
`uvm_field_int(pselx,UVM_ALL_ON)
`uvm_field_int(penable,UVM_ALL_ON)
`uvm_field_int(pwrite,UVM_ALL_ON)
`uvm_field_int(pwdata,UVM_ALL_ON)
`uvm_object_utils_end

//constructor
function new(string name="apb_seq_items");
super.new(name);
endfunction

constraint addr_ena {(paddr==1) -> penable;};

endclass
