
class AHB_S_Driver extends uvm_driver#(AHB_S_Seq_item);

        `uvm_component_utils(AHB_S_Driver)

        virtual ahb_intf.SDRV_MP vif;
        AHB_S_Agent_Config sagt_cfg;


        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_S_Driver", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);
        extern task run_phase(uvm_phase phase);
        extern task drive();
        extern task reset_();

endclass

        //Constructor
        function AHB_S_Driver::new(string name = "AHB_S_Driver", uvm_component parent);
                super.new(name, parent);
        endfunction

        //Build
        function void AHB_S_Driver::build_phase(uvm_phase phase);
                if(!uvm_config_db#(AHB_S_Agent_Config)::get(this, "", "AHB_S_Agent_Config", sagt_cfg))
                begin
                        `uvm_fatal(get_full_name(), "Cannot get VIF from configuration databse!")
                end

                super.build_phase(phase);
        endfunction

        //Connect
        function void AHB_S_Driver::connect_phase(uvm_phase phase);
                vif = sagt_cfg.vif;
        endfunction

        //Run
        task AHB_S_Driver::run_phase(uvm_phase phase);
                forever
                begin
                        seq_item_port.get_next_item(req);
                        fork
                                begin: drv
                                        drive();
                                        disable rst;
                                end
                                begin:rst
                                        reset_();
                                        disable drv;
                                end
                        join
                        seq_item_port.item_done(req);
                end
        endtask

        task AHB_S_Driver::drive();
        begin: driver
                if(req.response == ERROR)
                begin
                        vif.sdrv_cb.HRESP <= 1;
                        vif.sdrv_cb.HREADY <= 0;
                        @(vif.sdrv_cb);
                        vif.sdrv_cb.HREADY <= 1;
                        @(vif.sdrv_cb);
                end
                else
                begin
                        vif.sdrv_cb.HRESP <= 0;
                        foreach(req.ready[i])
                        begin
                                vif.sdrv_cb.HREADY <= req.ready[i];
                                if((vif.sdrv_cb.HREADY)&&(!vif.sdrv_cb.HWRITE)&&(vif.sdrv_cb.HTRANS != 0)&&(vif.sdrv_cb.HTRANS != 1))
                                        vif.sdrv_cb.HRDATA <= {$random};
                                if(vif.sdrv_cb.HSIZE > 2)
                                        vif.sdrv_cb.HRESP <= 1;
                                else vif.sdrv_cb.HRESP <= 0;

                                @(vif.sdrv_cb);
                        end
                        vif.sdrv_cb.HREADY <= 1'b1;
                end
        end: driver
        endtask

        task AHB_S_Driver::reset_();
                wait(!vif.HRESETn);
                        vif.sdrv_cb.HRESP <= 0;
                        vif.sdrv_cb.HREADY <= 1;
                        vif.sdrv_cb.HRDATA <= 0;
                @(vif.sdrv_cb);
        endtask


