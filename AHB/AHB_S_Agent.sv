
class AHB_S_Agent extends uvm_agent;

        `uvm_component_utils(AHB_S_Agent)


        AHB_S_Driver sdriver_h;
        AHB_S_Monitor smonitor_h;
        AHB_S_Sequencer sseqr_h;

        uvm_analysis_port#(AHB_S_Seq_item) agent_ap;

        AHB_S_Agent_Config sagt_cfg;
        uvm_active_passive_enum is_active;

        //-------------------------------------------------
        // Methods
        //-------------------------------------------------

        extern function new(string name = "AHB_S_Agent", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);

endclass

        //Constructor
        function AHB_S_Agent::new(string name = "AHB_S_Agent", uvm_component parent);
                super.new(name, parent);
                agent_ap = new("agent_ap", this);
        endfunction

        //Build
        function void AHB_S_Agent::build_phase(uvm_phase phase);
                if(!uvm_config_db#(AHB_S_Agent_Config)::get(this, "", "AHB_S_Agent_Config", sagt_cfg))
                begin
                        `uvm_fatal(get_full_name(), "Cannot get AGENT-CONFIG from configuration database!")
                end

                is_active = sagt_cfg.is_active;

                super.build_phase(phase);

                smonitor_h = AHB_S_Monitor::type_id::create("smonitor_h", this);
                if(is_active == UVM_ACTIVE)
                begin
                        sdriver_h = AHB_S_Driver::type_id::create("sdriver_h", this);
                        sseqr_h = AHB_S_Sequencer::type_id::create("sseqr_h", this);
                end
        endfunction

        //Connect
        function void AHB_S_Agent::connect_phase(uvm_phase phase);
                agent_ap.connect(smonitor_h.monitor_ap);

                if(is_active == UVM_ACTIVE)
                begin
                        sdriver_h.seq_item_port.connect(sseqr_h.seq_item_export);
                end
        endfunction



