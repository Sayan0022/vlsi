
class AHB_Env_Config1 extends uvm_object;

        `uvm_object_utils(AHB_Env_Config1)

        AHB_M_Agent_Config magt_cfg;
        AHB_S_Agent_Config sagt_cfg;

        uvm_active_passive_enum m_is_active;
        uvm_active_passive_enum s_is_active;

        virtual ahb_intf vif;


        //-------------------------------------------------
        // Methods
        //-------------------------------------------------

        extern function new(string name = "AHB_Env_Config1");

endclass

        //Constructor
        function AHB_Env_Config1::new(string name = "AHB_Env_Config1");
                super.new(name);
        endfunction


