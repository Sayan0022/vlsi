//`include "uvm_macros.svh"
//`include "AHB_M_Seq_item.sv"
//import uvm_pkg::*;

class AHB_M_sequencer extends uvm_sequencer#(AHB_M_Seq_item);

        `uvm_component_utils(AHB_M_sequencer)


        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_M_sequencer", uvm_component parent);

endclass

        //Constructor
        function AHB_M_sequencer::new(string name = "AHB_M_sequencer", uvm_component parent);
                super.new(name, parent);
        endfunction
