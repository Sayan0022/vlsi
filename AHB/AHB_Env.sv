
class AHB_Env extends uvm_env;

        `uvm_component_utils(AHB_Env)

    //    AHB_Env_Config1 env_cfg;
        AHB_M_Agent_Config magt_cfg;
        AHB_S_Agent_Config sagt_cfg;

        AHB_Reset_Agent reset_agent_h;
        AHB_M_Agent master_agent_h;
        AHB_S_Agent slave_agent_h;

        AHB_Coverage ahb_coverage_h;
        AHB_Vsequencer vseqr_h;

        uvm_analysis_port#(AHB_M_Seq_item) ahb_master_ap;
        uvm_analysis_port#(AHB_S_Seq_item) ahb_slave_ap;


        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_Env", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);
endclass

        //Constructor
        function AHB_Env::new(string name = "AHB_Env", uvm_component parent);
                super.new(name, parent);

                ahb_master_ap = new("ahb_master_ap", this);
                ahb_slave_ap = new("ahb_slave_ap", this);
        endfunction

        //Build
        function void AHB_Env::build_phase(uvm_phase phase);
                if(!uvm_config_db#(AHB_Env_Config1)::get(this, "", "AHB_Env_Config1", env_cfg))
                begin
                        `uvm_fatal(get_full_name(), "Cannot get ENV-CONFIG from configuration database!")
                end

                uvm_config_db#(virtual ahb_intf)::set(this, "reset_agent*", "reset_controller", env_cfg.vif);

                // Set master agent configuration       
                magt_cfg = AHB_M_Agent_Config::type_id::create("magt_cfg");
                magt_cfg.vif = env_cfg.vif;
                magt_cfg.is_active = env_cfg.m_is_active;
                uvm_config_db#(AHB_M_Agent_Config)::set(this, "master_agent*", "AHB_M_Agent_Config", magt_cfg);


                // Set slave agent configuration        
                sagt_cfg = AHB_S_Agent_Config::type_id::create("sagt_cfg");
                sagt_cfg.vif = env_cfg.vif;
                sagt_cfg.is_active = env_cfg.s_is_active;
                uvm_config_db#(AHB_S_Agent_Config)::set(this, "slave_agent*", "AHB_S_Agent_Config", sagt_cfg);

                super.build_phase(phase);

                reset_agent_h = AHB_Reset_Agent::type_id::create("reset_agent_h", this);
                master_agent_h = AHB_M_Agent::type_id::create("master_agent_h", this);
                slave_agent_h = AHB_S_Agent::type_id::create("slave_agent_h", this);

                //ahb_coverage_h = AHB_Coverage::type_id::create("ahb_coverage_h", this);

                vseqr_h = AHB_Vsequencer::type_id::create("vseqr_h", this);

        endfunction

        //Connect
        function void AHB_Env::connect_phase(uvm_phase phase);
                master_agent_h.agent_ap.connect(ahb_coverage_h.analysis_export);
                master_agent_h.agent_ap.connect(ahb_master_ap);
                slave_agent_h.agent_ap.connect(ahb_slave_ap);

                vseqr_h.reset_seqr_h = reset_agent_h.reset_seqr_h;

                if(magt_cfg.is_active == UVM_ACTIVE)
                begin
                        vseqr_h.mseqr_h = master_agent_h.mseqr_h;
                end

                if(sagt_cfg.is_active == UVM_ACTIVE)
                begin
                        vseqr_h.sseqr_h = slave_agent_h.sseqr_h;
                end
        endfunction


