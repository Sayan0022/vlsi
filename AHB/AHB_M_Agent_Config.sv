//`include "uvm_macros.svh"
//import uvm_pkg::*;

class AHB_M_Agent_Config extends uvm_object;

        `uvm_object_utils(AHB_M_Agent_Config)

        virtual ahb_intf vif;
        uvm_active_passive_enum is_active;


        //-------------------------------------------------
        // Methods
        //-------------------------------------------------

        extern function new(string name = "AHB_M_Agent_Config");

endclass

        //Constructor
        function AHB_M_Agent_Config::new(string name = "AHB_M_Agent_Config");
                super.new(name);
        endfunction

