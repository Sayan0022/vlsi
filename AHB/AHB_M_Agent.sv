`include "uvm_macros.svh"
//`include "AHB_M_Driver.sv"
//`include "AHB_M_Monitor.sv"
//`include "AHB_M_sequencer.sv"
//import uvm_pkg::*;

class AHB_M_Agent extends uvm_agent;

        `uvm_component_utils(AHB_M_Agent)

        AHB_M_Driver mdriver_h;
        AHB_M_Monitor mmonitor_h;
        AHB_M_sequencer mseqr_h;

        uvm_analysis_port#(AHB_M_Seq_item) agent_ap;

        AHB_M_Agent_Config magt_cfg;

        uvm_active_passive_enum is_active;

        //--------------------------------------------
        // Methods
        //--------------------------------------------

        extern function new(string name = "AHB_M_Agent", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);

endclass

        //Constructor
        function AHB_M_Agent::new(string name = "AHB_M_Agent", uvm_component parent);
                super.new(name, parent);
                agent_ap = new("agent_ap", this);
        endfunction

        //Build
        function void AHB_M_Agent::build_phase(uvm_phase phase);
                if(!uvm_config_db#(AHB_M_Agent_Config)::get(this, "", "AHB_M_Agent_Config", magt_cfg))
                begin
                        `uvm_fatal(get_full_name(), "Cannot get AGENT-CONFIG from configuration database!")
                end

                is_active = magt_cfg.is_active;

                super.build_phase(phase);

                mmonitor_h = AHB_M_Monitor::type_id::create("mmonitor_h", this);
                if(is_active == UVM_ACTIVE)
                begin
                        mdriver_h = AHB_M_Driver::type_id::create("mdriver_h", this);
                        mseqr_h = AHB_M_sequencer::type_id::create("mseqr_h", this);
                end
        endfunction

        //Connect
        function void AHB_M_Agent::connect_phase(uvm_phase phase);
                mmonitor_h.monitor_ap.connect(agent_ap);

                if(is_active == UVM_ACTIVE)
                begin
                        mdriver_h.seq_item_port.connect(mseqr_h.seq_item_export);
                end
        endfunction


