class AHB_Reset_Base_Sequence extends uvm_sequence#(AHB_M_Seq_item);

        `uvm_object_utils(AHB_Reset_Base_Sequence)

        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_Reset_Base_Sequence");

endclass

        //Constructor
        function AHB_Reset_Base_Sequence::new(string name = "AHB_Reset_Base_Sequence");
                super.new(name);
        endfunction


//---------------------------------------------------------------
// Assert Reset Sequence
//---------------------------------------------------------------

class reset_seq extends AHB_Reset_Base_Sequence;

        `uvm_object_utils(reset_seq)


        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "reset_seq");
        extern task body();

endclass

        //Constructor
        function reset_seq::new(string name = "reset_seq");
                super.new(name);
        endfunction

        //Body
        task reset_seq::body();
                req = AHB_M_Seq_item::type_id::create("req");
                start_item(req);
                        assert(req.randomize() with {burst_mode == SINGLE;});
                        req.reset = 0;
                finish_item(req);
        endtask


//---------------------------------------------------------------
// De-assert Sequence
//---------------------------------------------------------------

class set_seq extends AHB_Reset_Base_Sequence;

        `uvm_object_utils(set_seq)


        //------------------------------------------------
        // Methods
        //------------------------------------------------
        extern function new(string name = "set_seq");
        extern task body();

endclass

        //Constructor
        function set_seq::new(string name = "set_seq");
                super.new(name);
        endfunction

        //Body
        task set_seq::body();
                req = AHB_M_Seq_item::type_id::create("req");
                start_item(req);
                        assert(req.randomize() with {burst_mode == SINGLE;});
                        req.reset = 1;
                finish_item(req);
        endtask

