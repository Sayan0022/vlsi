class AHB_S_Monitor extends uvm_monitor;

        `uvm_component_utils(AHB_S_Monitor)

        virtual ahb_intf vif;
        AHB_S_Agent_Config sagt_cfg;
        AHB_S_Seq_item xtn;

        uvm_analysis_port#(AHB_S_Seq_item) monitor_ap;


        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_S_Monitor", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);
        extern task run_phase(uvm_phase phase);
        extern task monitor();

endclass

        //Constructor
        function AHB_S_Monitor::new(string name = "AHB_S_Monitor", uvm_component parent);
                super.new(name, parent);

                monitor_ap = new("monitor_ap", this);

        endfunction

        //Build
        function void AHB_S_Monitor::build_phase(uvm_phase phase);
                if(!uvm_config_db#(AHB_S_Agent_Config)::get(this, "", "AHB_S_Agent_Config", sagt_cfg))
                begin
                        `uvm_fatal(get_full_name(), "Cannot get VIF from configuration database!")
                end

                super.build_phase(phase);
        endfunction

        //Connect
        function void AHB_S_Monitor::connect_phase(uvm_phase phase);
                vif = sagt_cfg.vif;
        endfunction

        //Run
        task AHB_S_Monitor::run_phase(uvm_phase phase);
                forever
                begin
                        fork
                                begin: mon
                                        monitor();
                                        disable wait_for_reset;
                                end
                                begin: wait_for_reset
                                        wait(!vif.HRESETn);
                                        xtn = AHB_S_Monitor::type_id::create("xtn");
                                        xtn.reset =  0;
                                        $cast(xtn.trans_type, vif.HTRANS);
                                        disable mon;
                                        monitor_ap.write(xtn);
                                        @(vif.smon_cb);
                                end
                        join
                end
        endtask

        //Monitor Bursts
        task AHB_S_Monitor::monitor();
        begin: mon1
                xtn = AHB_S_Seq_item::type_id::create("xtn");

                do
                begin: collect
                        $cast(xtn.trans_type, vif.mmon_cb.HTRANS);
                        $cast(xtn.burst_mode, vif.mmon_cb.HBURST);
                        $cast(xtn.trans_size, vif.mmon_cb.HSIZE);
                        $cast(xtn.read_write, vif.mmon_cb.HWRITE);
                        $cast(xtn.response, vif.mmon_cb.HRESP);
                        xtn.address.push_back(vif.mmon_cb.HADDR);

                        if((xtn.trans_type == IDLE) && (vif.HRESETn == 1))
                        begin
                                @(vif.mmon_cb);
                                `uvm_info(get_type_name(), "IDLE Transaction Detected..", UVM_MEDIUM)
                                monitor_ap.write(xtn);
                                disable mon1;
                        end
                        else
                        begin
                                @(vif.mmon_cb);
                                if(vif.HRESP == 1)
                                begin
                                        `uvm_info(get_type_name(), $sformatf("ERROR Detected on Address %h", xtn.address[$]), UVM_MEDIUM)
                                        disable collect;
                                end

                                while(!vif.HREADY || (vif.HTRANS == 1))
                                begin
                                        @(vif.mmon_cb);
                                        if(vif.HRESP == 1)
                                        begin
                                                `uvm_info(get_type_name(), $sformatf("ERROR Detected on Address %h", xtn.address[$]), UVM_MEDIUM)
                                                disable collect;
                                        end
                                end

                                if(xtn.read_write == READ)
                                        xtn.read_data.push_back(vif.mmon_cb.HRDATA);
                                else
                                        xtn.write_data.push_back(vif.mmon_cb.HWDATA);
                        end
                end: collect
                while(vif.HTRANS == 3);

                `uvm_info(get_type_name(), "Received Packet From AHB Slave Monitor..", UVM_MEDIUM)
                xtn.print();
                monitor_ap.write(xtn);
        end
        endtask


