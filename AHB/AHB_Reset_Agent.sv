
class AHB_Reset_Agent extends uvm_agent;

        `uvm_component_utils(AHB_Reset_Agent)

        AHB_Reset_Driver driver_h;
        AHB_Reset_Sequencer reset_seqr_h;

        //--------------------------------------------
        // Methods
        //--------------------------------------------

        extern function new(string name = "AHB_Reset_Agent", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);

endclass

        //Constructor
        function AHB_Reset_Agent::new(string name = "AHB_Reset_Agent", uvm_component parent);
                super.new(name, parent);
        endfunction

        //Build
        function void AHB_Reset_Agent::build_phase(uvm_phase phase);
                super.build_phase(phase);

                driver_h = AHB_Reset_Driver::type_id::create("driver_h", this);
                reset_seqr_h = AHB_Reset_Sequencer::type_id::create("reset_seqr_h", this);
        endfunction

        //Connect
        function void AHB_Reset_Agent::connect_phase(uvm_phase phase);
                driver_h.seq_item_port.connect(reset_seqr_h.seq_item_export);
        endfunction




