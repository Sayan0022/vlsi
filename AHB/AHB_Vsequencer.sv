
class AHB_Vsequencer extends uvm_sequencer#(uvm_sequence_item);

        `uvm_component_utils(AHB_Vsequencer)


        AHB_Reset_Sequencer reset_seqr_h;
        AHB_M_sequencer mseqr_h;
        AHB_S_Sequencer sseqr_h;

        //-------------------------------------------------
        // Methods
        //-------------------------------------------------

        extern function new(string name = "AHB_Vsequencer", uvm_component parent);

endclass

        //Constructor
        function AHB_Vsequencer::new(string name = "AHB_Vsequencer", uvm_component parent);
                super.new(name, parent);
        endfunction


