`include "uvm_macros.svh"
//`include "AHB_M_Seq_item.sv"
//`include "AHB_M_Agent_Config.sv"
//import uvm_pkg::*;

class AHB_M_Driver extends uvm_driver#(AHB_M_Seq_item);

        `uvm_component_utils(AHB_M_Driver)

        virtual ahb_intf vif;
        AHB_M_Agent_Config magt_cfg;

        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_M_Driver", uvm_component parent);
        extern function void build_phase(uvm_phase phase);
        extern function void connect_phase(uvm_phase phase);
        extern task run_phase(uvm_phase phase);
        extern task drive();

endclass

        //Constructor
        function AHB_M_Driver::new(string name = "AHB_M_Driver", uvm_component parent);
                super.new(name, parent);
        endfunction

        //Build
        function void AHB_M_Driver::build_phase(uvm_phase phase);
                if(!uvm_config_db#(AHB_M_Agent_Config)::get(this, "", "AHB_M_Agent_Config", magt_cfg))
                begin
                        `uvm_fatal(get_full_name(), "Cannot get VIF from configuration database!")
                end

                super.build_phase(phase);
        endfunction

        //Connect
        function void AHB_M_Driver::connect_phase(uvm_phase phase);
                vif = magt_cfg.vif;
        endfunction

        //Run
        task AHB_M_Driver::run_phase(uvm_phase phase);
                forever
                begin
                        seq_item_port.get_next_item(req);
                        void'(req.add_busy());
                        fork
                                begin: driver
                                        wait(vif.HRESETn);
                                        drive();
                                        disable resp;
                                        disable rst;
                                end
                                begin:resp
                                        wait(vif.HRESETn);
                                        forever
                                        begin
                                                if((vif.HRESP == 1) && (vif.HTRANS == 0))
                                                        @(vif.mdrv_cb);
                                                else
                                                begin
                                                        wait(vif.mdrv_cb.HRESP == 1);
                                                        disable driver;
                                                        vif.mdrv_cb.HTRANS <= 0;
                                                        @(vif.mdrv_cb);
                                                        disable rst;
                                                        break;
                                                end
                                        end
                                end
                                begin:rst
                                        forever
                                        begin
                                                wait(!vif.HRESETn);
                                                `uvm_info(get_type_name(), "RESET Detected..", UVM_MEDIUM)
                                                disable driver;
                                                disable resp;
                                                vif.HTRANS <= 0;
                                                vif.HBURST <= 0;
                                                vif.HSIZE <= 0;
                                                vif.HWRITE <= 0;
                                                vif.HADDR <= 0;
                                                vif.HWDATA <= 0;

                                                @(vif.mdrv_cb);
                                                if(vif.HRESETn)
                                                        disable rst;
                                        end
                                end

                        join
                        seq_item_port.item_done(req);
                end
        endtask

        //Drive
        task AHB_M_Driver::drive();
        begin: drv

                int j;          //Variable to Increment HTRANS Queue
                bit busy_last;

                        `uvm_info(get_type_name(), "Transaction From AHB Master Driver..", UVM_MEDIUM)
                        req.print();

                        //Drive Controls
                        vif.mdrv_cb.HBURST <= req.burst_mode;
                        vif.mdrv_cb.HSIZE <= req.trans_size;
                        vif.mdrv_cb.HWRITE <= req.read_write;


                        //Drive Address, Transaction Type and Data
                        foreach(req.address[i])
                        begin
                                vif.mdrv_cb.HADDR <= req.address[i];
                                if(busy_last)
                                begin
                                        @(vif.mdrv_cb);
                                        busy_last = 0;
                                end
                                vif.mdrv_cb.HTRANS <= req.trans_type[j];
                                if(req.trans_type[j] == 2'b01)
                                begin
                                        do
                                        begin
                                                j++;
                                                @(vif.mdrv_cb);
                                                vif.mdrv_cb.HTRANS <= req.trans_type[j];
                                        end
                                        while(req.trans_type[j] == 2'b01);
                                end
                                @(vif.mdrv_cb);
                                j++;

                                while(!vif.mdrv_cb.HREADY) @(vif.mdrv_cb);
                                if(req.read_write)
                                begin
                                        vif.mdrv_cb.HWDATA <= req.write_data[i];
                                end
                        end
                     /*   if((req.burst_mode == INCR)&&(req.trans_type[j] == 1))
                        begin
                                vif.mdrv_cb.HTRANS <= 1'b1;     //Drive Busy at End
                                busy_last = 1;
                        end*/
        end
        endtask


