
class AHB_Reset_Sequencer extends uvm_sequencer#(AHB_M_Seq_item);

        `uvm_component_utils(AHB_Reset_Sequencer)


        //------------------------------------------------
        // Methods
        //------------------------------------------------

        extern function new(string name = "AHB_Reset_Sequencer", uvm_component parent);

endclass

        //Constructor
        function AHB_Reset_Sequencer::new(string name = "AHB_Reset_Sequencer", uvm_component parent);
                super.new(name, parent);
        endfunction


